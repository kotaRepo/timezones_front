import React from 'react';
import './style.css'

const TimeZones = ({
  timeZones=[],
  onClickZone,
}) => {
  return (
    <>
    <div className="timezonelist">
    { timeZones.map((data,index) => {
      return (
      <div
        className="zoneItem"
        onClick={(e) => onClickZone(e, e.target.innerText)}>
          {data.name}
      </div>
          )
    })}
    </div>
    </>
  );
}

export default TimeZones
