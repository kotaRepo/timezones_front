import React, { useState, useEffect } from 'react';
import SearchBar from '../SearchBar/index.js';
import TimeZones from '../TimeZones/index.js';
import { BASE_PATH_SERVER } from '../../utils/constants';
import './style.css';

let arrayBoxes = [];

const SearchPage = (props) => {
  const [input, setInput] = useState('');
  const [timeZonesDefault, setTimeZonesDefault] = useState();
  const [timeZones, setTimeZones] = useState();
  const [showTimeZones, setShowTimeZones] = useState(false);
  const [timeZonesSelected, setTimeZonesSelected] = useState();
  
  //Get data first time use and save 
  const fetchDataFTU = async () => {
      return await fetch(BASE_PATH_SERVER)
      .then(response => response.json())
      .then(data => {
         setTimeZones(data) 
         setTimeZonesDefault(data)
       });
  }
  // Get current time zone and its local time
  const getTimeZone = name => {
    arrayBoxes.push(name);
    setTimeZonesSelected(arrayBoxes);
    
  /*return await fetch(`${BASE_PATH_SERVER}/${name}`)
  .then(response => response.json())
  .then(data => {
    arrayBoxes.push(data);
    setTimeZonesSelected(arrayBoxes);
   });*/
  }

  const updateInput = input => {
     const filtered = timeZonesDefault.filter(zones => {
      return zones.name.toLowerCase().includes(input.toLowerCase())
     })
     setInput(input);
     setTimeZones(filtered);
     setShowTimeZones(true);
  }

  const updateTextInput = input => {
    /*setInput(input);
    setTimeZones([]);
    setShowTimeZones(true);*/
  }

 const updateBoxes = (e, input) => {
    setInput(input);
    setTimeZones([]);
    setShowTimeZones(true);
    getTimeZone(input);
 }

  useEffect( () => {fetchDataFTU()},[]);
	
  return (
    <>
      <h1>Time Zones</h1>
      <div className="main">
      <div>
        <SearchBar 
        input={input} 
        onChangeInput={updateInput}
        onClickInput={updateTextInput}
        />
      </div>
      <div className="zones">
        {showTimeZones && (
          <TimeZones 
          onClickZone={updateBoxes}
          timeZones={timeZones}/>
        )}
      </div>
      <div className="container">
      
        {timeZonesSelected && timeZonesSelected.map( (data,index) => {
            return (
              <div className="boxes">
              <button className="close">X</button>
              {data}
              </div>
            )
        })}
      </div>
      </div>
    </>
   );
}

export default SearchPage